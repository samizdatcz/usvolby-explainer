;(function(){
  var votes;

  var years = [];
  var maps = {};
  var mapWithData = {};

  $(".map").each(function() {
    var year = $(this).data("year");
    years.push(year);
    maps[year] = L.map($(this).get()[0], {scrollWheelZoom: false}).setView([39.82, -98.58], 4);
    L.esri.basemapLayer("Topographic").addTo(maps[year]);
  });

  $.getJSON('./data/votes.json', function(data) {
    votes = data;
  });



  //obsluha topojsonu, Copyright (c) 2013 Ryan Clark

  L.TopoJSON = L.GeoJSON.extend({
    addData: function(jsonData) {
      if (jsonData.type === "Topology") {
        for (key in jsonData.objects) {
          geojson = topojson.feature(jsonData, jsonData.objects[key]);
          L.GeoJSON.prototype.addData.call(this, geojson);
        }
      }
      else {
        L.GeoJSON.prototype.addData.call(this, jsonData);
      }
    }
  });



  // načtení dat

  for(i = 0; i < years.length; i++) {
    mapWithData[years[i]] = new L.TopoJSON(null, {
      style: function(feature) {
        return setStyleByYear(years[i], feature);
      },
      onEachFeature: function(feature, layer) {
        return onEachFeature(years[i], feature, layer);
      }
    });
  }

  $.getJSON('./data/states.topo.json')
    .done(addTopoData);

  function addTopoData(topoData){
    for(i = 0; i < years.length; i++) {
      mapWithData[years[i]].addData(topoData);
      mapWithData[years[i]].addTo(maps[years[i]]);
    }
  };

  function getResultsForDemAndRep(data) {
      var dem_result = 0
      var rep_result = 0;
      var sou_result = 0
      var con_result = 0;

      var variant = 0;

      if(data) {

        if(typeof data.pct_dem !== 'undefined') {
          dem_result = data.ev_dem != null ? data.ev_dem : 0;
          rep_result = data.ev_rep != null ? data.ev_rep : 0;
          variant = 1;
        }
        else {
          if(typeof data.ev_dem !== 'undefined') {
            dem_result = data.ev_dem != null ? data.ev_dem : 0;
            rep_result = data.ev_rep != null ? data.ev_rep : 0;
            sou_result = data.ev_sou != null ? data.ev_sou : 0;
            con_result = data.ev_con != null ? data.ev_con : 0;
            variant = 2;
          }
          else {
            dem_result = data.ev_demrep != null ? data.ev_demrep : 0;
            rep_result = data.ev_fed != null ? data.ev_fed : 0;
            variant = 3;
          }

        }
      }

      if(dem_result == 0 && rep_result == 0 && sou_result == 0 && con_result == 0) variant = 0;

      return {dem: dem_result, rep: rep_result, sou: sou_result, con: con_result, variant: variant}
  }



  // obarvení

  function setStyleByYear(year, feature) {

    var state = feature.properties.NAME;
    var color = "grey";

    if(votes[state] && votes[state][year]) {

      var data = getResultsForDemAndRep(votes[state][year])

      if(data.variant == 3) {
        color = data.dem > data.rep ? '#2ECC40' : '#FF851B'
      } else if (data.variant == 2) {
          switch(Math.max(data.dem, data.rep, data.sou, data.con)) {
            case(data.dem): color = '#0074D9'; break;
            case(data.rep): color = '#FF4136'; break;
            case(data.sou): color = '#2ECC40'; break;
            case(data.con): color = '#FF851B'; break;
          }
      } else {
        color = data.dem > data.rep ? '#0074D9' : '#FF4136'
      }
    }

    return {
      weight: 1,
      opacity: 1,
      color: 'grey',
      dashArray: '0',
      fillOpacity: 0.7,
      fillColor: color
    };
  }


  // interaktivita

  function highlightFeature(e, year) {
    var layer = e.target;

    updateInfoBox(info[year], year, layer.feature.properties);
  }

  function resetBox(year) {
    if(year==1800) {
      info[year]._div.innerHTML = '<h4>Celkové výsledky (1800)</h4><div class=demrep><b>Demokraticky-republikánská strana 73 volitelů</div><div class=fed>Federalistická strana 65 volitelů</b></div>';
    } else if(year==1860) {
      info[year]._div.innerHTML = '<h4>Celkové výsledky (1860)</h4><div class=dem><b>Severní demokraté 12 volitelů</div><div class=rep>Republikáni 180 volitelů</div><div class=sou>Jižní demokraté 72 volitelů</div><div class=con>Konstituční strana 39 volitelů</div>';
    } else if(year==1896) {
      info[year]._div.innerHTML = '<h4>Celkové výsledky (1896)</h4><div class=dem><b>Demokraté 176 volitelů (46,7 %)</div><div class=rep>Republikáni 271 volitelů (51,0 %)</div>';
    } else if(year==1932) {
      info[year]._div.innerHTML = '<h4>Celkové výsledky (1932)</h4><div class=dem><b>Demokraté 472 volitelů (57,4 %)</div><div class=rep>Republikáni 59 volitelů (39,7 %)</div>';
    } else if(year==1964) {
      info[year]._div.innerHTML = '<h4>Celkové výsledky (1964)</h4><div class=dem><b>Demokraté 486 volitelů (61,1 %)</div><div class=rep>Republikáni 52 volitelů (38,5 %)</div>';
    } else if(year==1980) {
      info[year]._div.innerHTML = '<h4>Celkové výsledky (1980)</h4><div class=dem><b>Demokraté 49 volitelů (41,0%)</div><div class=rep>Republikáni 489 volitelů (50,7 %)</div>';
    } else {
      info[year]._div.innerHTML = '<h4>Celkové výsledky (1800)</h4><div class=dem><b>Demokraté 266 volitelů (48,4%)</div><div class=rep>Republikáni 271 volitelů (47,9 %)</div>';
    }
  };

  function onEachFeature(year, feature, layer) {
   layer.on({
     mouseover: function(ev) {
       return highlightFeature(ev, year);
     },
     mouseout: function(ev) {
      resetBox(year);
     }
   });
  }


  var sklonujVolitele = function (pocet) {
     var tvar = "volitelů";
     if(pocet == 1) {
      tvar = "volitel";
     }
     else if(pocet < 5 && pocet != 0) {
      tvar =  "volitelé";
     }
     
     return pocet + " " + tvar;
  }

  var updateInfoBox = function (t, year, props) {

    if(props) {

      var state = props['NAME'];

      var data = getResultsForDemAndRep(votes[state][year]);

      if(data.variant == 3) {
        t._div.innerHTML = '<h4>' + props['NAME'] + '</h4>' + '<div class=demrep>Demokraticky-republikánská strana ' + sklonujVolitele(data.dem) + '</div>' + '<div class=fed>Federalistická strana ' + sklonujVolitele(data.rep) + ' </div>';
      } else if(data.variant == 2) {
        t._div.innerHTML = '<h4>' + props['NAME'] + '</h4>' + '<div class=dem>Severní demokraté ' + sklonujVolitele(data.dem) + '</div>' + '<div class=rep>Republikáni ' + sklonujVolitele(data.rep) + '</div>' + '<div class=sou>Jižní demokraté ' + sklonujVolitele(data.sou) + ' </div>' + '<div class=con>Konstituční strana ' + sklonujVolitele(data.con) + '</div>';
      } else if(data.variant == 1) {
        t._div.innerHTML = '<h4>' + props['NAME'] + '</h4>' + '<div class=dem>Demokraté ' + sklonujVolitele(data.dem) + ' (' + votes[state][year]['pct_dem'] + ' % hlasů)</div>' + '<div class=rep>Republikáni ' + sklonujVolitele(data.rep) + ' (' + votes[state][year]['pct_rep'] + ' % hlasů)</div>';
      } else {
        t._div.innerHTML = 'Pro zvolený stát nejsou data'
      }
    }
  };

  var info = {};

  for(i = 0; i < years.length; i++) {
    info[years[i]] = L.control();
    info[years[i]].onAdd = function (map) {
      this._div = L.DomUtil.create('div', 'info');
      resetBox(years[i])
      return this._div;
    };

    info[years[i]].addTo(maps[years[i]]);

  }
}).call(this);
