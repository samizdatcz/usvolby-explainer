---
title: "7 historických volebních momentů, které dovedly Ameriku k Trumpovi"
perex: "Jak se vyvíjela americká volební mapa, kdy si demokratická a republikánská strana vyměnily pozice a co vlastně předcházelo jejich založení? Vybrali jsme pro vás sedm klíčových prezidentských voleb, které určily i podobu klání, v němž zvítězil Donald Trump."
description: "Jak se vyvíjela americká volební mapa, kdy si demokratická a republikánská strana vyměnily pozice a co vlastně předcházelo jejich založení? Vybrali jsme pro vás sedm klíčových prezidentských voleb, které určují podobu letošního klání mezi Clintonovou a Trumpem."
authors: ["Michal Zlatkovský", "Jan Boček", "Jan Cibulka", "Jan Pospíšil"]
published: "31. října 2016"
coverimg: https://interaktivni.rozhlas.cz/usvolby-explainer/media/cover.jpg
coverimg_note: "Donald Trump, foto <a href='https://commons.wikimedia.org/wiki/File:Donald_Trump_(24877177839).jpg'>Gage Skidmore</a>, CC BY-SA 2.0"
socialimg: https://interaktivni.rozhlas.cz/usvolby-explainer/media/socialimg.jpg
url: "usvolby-explainer"
libraries: [jquery]
recommended:
  - link: https://interaktivni.rozhlas.cz/usa-twitter/
    title: Tři reportéři na cestě předvolební Amerikou. Sledujte je krok za krokem
    perex: Jde vlastně o rychlé zápisky z cest. Anebo chcete-li: o cestopis věku sociálních médií. Nabízíme twitterový záznam reportážní cesty zpravodajů Českého rozhlasu, kteří v USA sledují volební zápas mezi Hillary Clintonovou a Donaldem Trumpem.
    image: https://samizdat.cz/data/usa-tweety/www/media/socimg.jpg
  - link: http://www.rozhlas.cz/zpravy/data/_zprava/jizni-mesto-sidliste-bez-lidi--1532519
    title: Jižní Město – sídliště bez lidí?
    perex: Skoro deset tisíc lidí zmizelo z největšího tuzemského sídliště od roku 2001. Kam a proč? To je otázka důležitá pro všechny obyvatele paneláků. Podle posledního sčítání jich je v Česku 2,76 milionu. Mají ze svých sídlišť radši také rychle zmizet?
    image: http://media.rozhlas.cz/_obrazek/3467578.jpeg
  - link: https://interaktivni.rozhlas.cz/sudety/
    title: Existují Sudety? Hranice jsou zřetelné i sedmdesát let po vysídlení Němců
    perex: Statistiky nezaměstnanosti, kvality vzdělání nebo volební účasti v Česku dodnes kopírují hranice historických Sudet. Vlnu osadníků připomíná také nejsilnější příhraniční menšina – Slováci. Prohlédněte si detailní mapy.
    image: https://samizdat.cz/data/sudety-clanek/www/media/slovaci.jpg
  - link: http://www.rozhlas.cz/zpravy/data/_zprava/rusove-proti-rusum-zeme-v-nevyhlasene-obcanske-valce--1484099
    title: Rusové proti Rusům: Země v nevyhlášené občanské válce
    perex: Rusko prošlo v devadesátých letech demografickou krizí srovnatelnou s válečným konfliktem. Za Putinovy vlády se pád do propasti zpomalil, snad i zastavil. To ale nemusí vydržet dlouho: nynější růst stojí na velmi křehkých základech.
    image: http://media.rozhlas.cz/_obrazek/3372356.jpeg
---

V loňských amerických volbách bylo ve většině států USA o převaze demokratů Hillary Clintonové nebo republikánů Donalda Trumpa předem jasno. Momentální rozložení sil je určeno dlouholetým spletitým procesem. Jak a proč se vyvíjela americká volební mapa? Kdy si demokraté a republikáni vyměnili pozice a co vlastně jejich soupeření předcházelo? Vybrali jsme pro vás sedm klíčových prezidentských voleb, které předcházely Trumpově triumfu.

<div data-bso=1></div>

## 1. Volby 1800: Revoluce na přelomu století

Otcové zakladatelé Spojených států na konci osmnáctého století žádnou demokratickou ani republikánskou stranu neznali. Odmítali dokonce politické strany jako takové: americká ústava stranický systém vůbec nezmiňuje a první prezident USA George Washington se bál, že politická uskupení rozdělí společnost a zpomalí pokrok.

<figure>
<img src="media/washington.jpg" width="100%" alt="Podpis americké ústavy na filadelfském konventu; na stupínku George Washington (autor: Howard C. Christy, 1940)" title="Podpis americké ústavy na filadelfském konventu; na stupínku George Washington (autor: Howard C. Christy, 1940)">
<figcaption>
<small><i>
Podpis americké ústavy na filadelfském konventu; na stupínku George Washington (autor: Howard C. Christy, 1940)
</i></small>
</figcaption>
</figure>

Přesto se kolem dvou významných osobností té doby utvořila uskupení, ze kterých se brzy staly znesvářené tábory. Federalistická strana Alexandra Hamiltona, kterou založili především bankéři a byznysmeni, podporovala silnou federální vládu nad tehdejší patnáctkou amerických států. Demokraticky-republikánská strana, která vznikla okolo Hamiltonova odpůrce Thomase Jeffersona, oproti tomu stála za zájmy farmářů a odmítala větší centralizaci - a například i federální armádu. Obě strany také stály na opačných pólech zahraniční politiky: v tehdy probíhajících konfliktech v Evropě federalisté podporovali Británii a její aristokracii, zatímco demokratičtí republikáni se zhlédli ve francouzské revoluci. K jedněm nebo druhým se postupně přidali prakticky všichni dřívější nestraníci.

Počáteční převahu federalistů zvrátily čtvrté prezidentské volby, kterým se přezdívá __revoluce roku 1800__. Kampaň, která jim předcházela, se příliš nelišila od té, kterou Spojené státy zažívají dnes. Protivníci se vzájemně nálepkovali jako nebezpeční radikálové, kteří přinesou zemi zkázu. Politici sponzorovali noviny, aby psaly v jejich prospěch a očerňovaly jejich oponenty; platili také  „propagandisty“ - z dnešního pohledu politické marketéry - kteří vymýšleli úderné slogany, trefné urážky soupeřů i úplně nové propagační postupy, třeba distribuci předvolebních letáků. Obě strany navíc před volbami využívaly svou moc v jednotlivých státech ke změně volebních pravidel: někde se volitelé nevybírali ve volbách, ale hlasováním ve státních parlamentech, jinde se zase z většinového systému stal poměrný.

<aside class="big">
  <div class="map" data-year="1800" id="map1800"></div>
</aside>

Ve volbách nakonec zvítězili demokratičtí republikáni; v tom, kdo se vlastně stane prezidentem, ovšem ještě jasno nebylo. Nedomyšlená, až později vyjasněná ústavní pravidla - volitelé neměli jeden hlas, ale dva, přičemž nejúspěšnější kandidát se stal prezidentem a druhý v pořadí viceprezidentem - totiž způsobila, že Thomas Jefferson a jeho zamýšlený viceprezident Aaron Burr dostali stejný počet hlasů. Rozhodnout mezi nimi tak musel až Kongres, kterému se to po týden dlouhém zasedání podařilo až na třicátý šestý pokus. Jeffersonovo vítězství znamenalo postupný konec federalistů, které Američané čím dál silněji považovali za od lidu odtržené elity. Demokraticko-republikánská strana - a její přímý nástupce, dnešní __Demokratická strana__, která vznikla v roce 1828 - ovládla Spojené státy na dalších šedesát let.

<p class="boxik">
<b>Jak fungují americké prezidentské volby?</b><br>
Na rozdíl od začátků americké demokracie je dnes systém prezidentských voleb jasně daný. Z takzvaných <b>primárek</b> - <a class="boxlink" href="http://www.rozhlas.cz/zpravy/data/_zprava/klikata-cesta-do-bileho-domu-zacina-primarkami-v-iowe-proc-jsou-tak-dulezite--1579586">volebních předkol</a>, ve kterých demokraté i republikáni zvolí z uchazečů jednoho nejlepšího - vzejdou dva kandidáti, kteří se utkají v samotné volbě prezidenta, která se koná každé čtyři roky vždy v úterý po prvním listopadovém pondělí, letos tedy 8. listopadu. <br><br>

Američané nevolí přímo, ale každému z 50 států (a okrsku District of Columbia s hlavním městem Washingtonem) je podle počtu obyvatel přiřazen určitý počet <b>volitelů</b>. Voliči v jednotlivých státech hlasem pro kandidáta vlastně vybírají volitele, kteří se přikloní na jeho stranu. Platí přitom pravidlo „vítěz bere vše“: všichni volitelé připadají kandidátovi, který v daném státě zvítězí. (Výjimkou jsou státy Nebraska a Maine, které jsou rozdělené na okrsky.) <br><br>

Volitelů je aktuálně celkem 538 a aby se kandidát stal prezidentem, musí získat nadpoloviční většinu, tedy 270 volitelských hlasů. Samotní volitelé se scházejí až v prosinci, ale výsledek se považuje za jasně daný už od listopadových voleb: i když nejsou volitelé ve svém hlasování pro kandidáta vázáni zákonem, výsledky voleb nerespektují naprosto výjimečně. V případě, že by ani jeden kandidát nadpoloviční většinu hlasů nezískal, volí prezidenta parlament.<br><br>

Nepřímý volební systém se může zdát jako formalita, ale už ve čtyřech případech zvrátil výsledek, který by určila přímá volba. Naposledy se to stalo v roce 2000 v souboji mezi republikánem Georgem W. Bushem a demokratem Alem Gorem. Gore získal v mimořádně těsných volbách o půl milionu hlasů víc než Bush - jenže kvůli nepřímému systému získal o pět volitelů méně než Bush, který se po soudních přích ohledně přepočítávání výsledků skutečně stal prezidentem.</p>

## 2. Volby 1860: Abraham Lincoln, prezident do těžkých časů

Ameriku v dějinách nic nerozdělilo tolik jako otázka otroctví. Nakonec vedla až k rozpadu Spojených států a občanské válce; ještě předtím ovšem zapříčinila pád do té doby jednotné vládnoucí Demokratické strany. Když demokratický prezident James Buchanan před klíčovými volbami v roce 1860 otevřeně podpořil rozsudek upírající černochům americké občanství, demokraté se rozštěpili na dvě frakce. Severní protiotrocká i jižní prootrocká část strany do voleb nominovaly své vlastní kandidáty.

<aside class="big">
  <div class="map" data-year="1860" id="map1860"></div>
</aside>

<aside class="small">
<figure>
<img src="media/lincoln.jpg" width="100%" alt="Abraham Lincoln (autor: Mathew Brady, 1861)" title="Abraham Lincoln (autor: Mathew Brady, 1861)">
<figcaption>
<small><i>
Abraham Lincoln (autor: Mathew Brady, 1861)
</i></small>
</figcaption>
</figure>
</aside>

Do boje o prezidentské křeslo se k rozpolceným demokratům přidala do té doby neúspěšná nová strana - __republikáni__. Teprve šest let staré politické hnutí, které vzniklo na odpor proti otrokářům skupujícím půdu obyčejných farmářů, brzy získalo na oblibě nejen na americkém severu, ale i v nových západních státech. Do voleb v roce 1860 vyslali republikáni umírněného, ale schopného řečníka, právníka s politickou praxí Abrahama Lincolna. Ten i s minimální podporou na otrokářském jihu dokázal demokraty snadno porazit. Jeho vítězství ovšem vedlo k úplnému rozdělení národa. Po Lincolnově zvolení vyhlásilo sedm jižních otrokářských států odtržení od USA a založilo vlastní Konfederované státy americké. Lincolnova snaha udržet pevnosti na území Konfederace v rukou Spojených států vedla k prvním bojům, které vyústily v krvavou občanskou válku.

Tehdejší situace může z dnešního pohledu vypadat paradoxně: Demokraté, kteří v současnosti vyznávají liberalismus, hlásají před občanskou válkou malý stát a rozšíření otroctví, zatímco republikáni, dnes zastoupeni Donaldem Trumpem, bojují za utlačované černochy a široké státní regulace. Základní postoje obou stran se definitivně proměnily až více než století po občanské válce - názorový posun demokratů ovšem začíná dříve, na konci 19. století.

## 3. Volby 1896: Jak demokraté objevili populismus

V roce 1896 se Spojené státy nacházejí uprostřed hluboké ekonomické krize. Nezaměstnanost vyšplhala v některých státech až na třicet procent, krachují banky i malí podnikatelé a městy otřásají násilné demonstrace. V tak vypjaté situaci probíhá volební kampaň, o které se dodnes mluví jako o jedné z nejdramatičtějších vůbec.

<aside class="big">
  <div class="map" data-year="1896" id="map1860"></div>
</aside>

<aside class="small">
<figure>
<img src="media/bryan.png" width="100%" alt="William Jennings Bryan po svém projevu Kříž ze zlata (William Robinson Leigh, publikováno 1900)" title="William Jennings Bryan po svém projevu Kříž ze zlata (William Robinson Leigh, publikováno 1900)">
<figcaption>
<small><i>
William Jennings Bryan po svém projevu Kříž ze zlata (autor: William R. Leigh, publikováno 1900)
</i></small>
</figcaption>
</figure>
</aside>

Kvůli vleklým ekonomickým potížím došla Demokratické straně trpělivost s konzervativním probyznysovým křídlem, ke kterému patřil stávající prezident Grover Cleveland. Proti němu stáli zejména farmáři z chudého amerického jihu a západu - a v jejich čele levicový populista, teprve šestatřicetiletý William Jennings Bryan, zdatný řečník, jehož postoje ovlivní demokraty na dalších sedmdesát let. Jeho půlhodinový projev __Kříž ze zlata__, ve kterém zatratil konzervativními demokraty prosazovaný zlatý standard a zasadil se za krytí americké měny stříbrem, nadchnul straníky tak, že zprvu nenápadného outsidera vybrali za svého kandidáta.

Republikáni proti Bryanovi postavili veterána z občanské války Williama McKinleyho, který si jako šéfa kampaně najal schopného byznysmena Marka Hannu. Ten osobně obešel ředitele velkých korporací a bank, kteří dostali z Bryanova populismu strach - a vybral od nich na kampaň bezprecedentní částku, přepočteno na dnešní hodnotu peněz asi 85 milionů dolarů. Ty během tří měsíců utratil za řečníky, demonstrace a zejména stamiliony letáků, které slibovaly stabilitu a prosperitu pod McKinleym a vyhrožovaly zkázou a anarchií, které by přineslo zvolení Bryana.

Demokrat Bryan, od kterého se odvrátili tradiční straničtí sponzoři a který měl oproti McKinleymu minimum prostředků, zvolil jinou, do té doby nevídanou cestu. Během sta dní najezdil po Spojených státech vlakem víc než 30 tisíc kilometrů a na svých zastávkách pronesl přes 500 projevů, které slyšelo v součtu několik milionů lidí. Spoléhal na to, že média, která se přikláněla k republikánům, budou muset o jeho úspěšných projevech a nadšených reakcích posluchačů informovat čtenáře, a že si tak získá publicitu i bez přízně vydavatelů.

Republikánské finance ale nakonec nad Bryanovou lidovostí zvítězily a prezidentem se stal McKinley. Jeho konzervativní ekonomické postoje se ukázaly jako opodstatněné, vyvedly Spojené státy z krize a zajistily republikánům dominanci na dalších několik desetiletí. William Jennings Bryan ale tvář demokratů změnil už napořád. Ve straně byl Bryan aktivní dalších třicet let a stal se první americkou „politickou celebritou“. Bojoval proti zvyšujícímu se vlivu velkého byznysu a bank a stal se vůdčí tváří liberalismu a levicové politiky. Na základech, které položil, stavěli demokratičtí prezidenti své reformy i v šedesátých letech dvacátého století.

## 4. Volby 1932: Nový úděl Ameriky
Velký úspěch demokratů a jejich liberálního proudu ovšem přišel až ve třicátých letech minulého století, paradoxně s další zásadní krizí. V roce 1929 se ze dne na den propadly akcie na americké burze a na Spojené státy dolehla Velká hospodářská krize. Během několika let stoupla nezaměstnanost v USA až na 25 procent a statisíce lidí se ocitly bez domova. Hlavním viníkem krize byl podle mnohých Američanů republikánský prezident Herbert Hoover, jehož neúspěšná záchranná opatření z něj udělala terč posměchu a nenávisti. Volby roku 1932 se konaly v atmosféře nejhlubší společenské deprese.

<aside class="big">
  <div class="map" data-year="1932" id="map1932"></div>
</aside>

<aside class="small">
<figure>
<img src="media/fdr.jpg" width="100%" alt="Franklin D. Roosevelt v roce 1933 (foto: Elias Goldensky/Library of Congress)" title="Franklin D. Roosevelt v roce 1933 (foto: Elias Goldensky/Library of Congress)">
<figcaption>
<small><i>
Franklin D. Roosevelt v roce 1933 (foto: Elias Goldensky/Library of Congress)
</i></small>
</figcaption>
</figure>
</aside>

Hooverovi, který se rozhodl znovu kandidovat jednak kvůli osobní cti, jednak ze strachu z radikálního řešení krize jeho možnými republikánskými nástupci, se postavil guvernér státu New York, demokrat Franklin Delano Roosevelt. Za mohutné podpory své strany se prohlásil za kandidáta změny, a ačkoliv sám neměl příliš jasno v tom, jak onu změnu vlastně provést, jeho snadnému zvolení to nezabránilo: Hooverova nepopularita byla tak velká, že při svých pokusech o veřejná vystoupení běžně končil jako terč předmětů házených rozezleným davem a jeho ochranka musela zabránit hned několika pokusům o atentát.

Po svém zvolení začal Roosevelt - a zejména široký kruh jeho oficiálních i neformálních poradců - okamžitě připravovat sérii ekonomických opatření, která měla pomocí státních zásahů znovu oživit skomírající americké hospodářství. Sada reforem dostala název __New Deal__ (Nový úděl) - a její úspěch byl nakonec tak velký, že podnítila vznik nadstranické „koalice Nového údělu“, zajistila Rooseveltovi další tři volební vítězství a demokratům upevnila vůdčí pozici v americké politice až do šedesátých let.

Právě v Rooseveltově době se také zrodilo současné americké dělení politické scény na liberály a konzervativce. Liberálové byli ti, kteří dohodu New Deal podporovali, konzervativci byli její oponenti. V demokratické i republikánské straně přitom existovala konzervativní i liberální křídla, která nezřídka hlasovala spolu, a ne podle stranické příslušnosti.

<p class="boxik">
<b>Konzervatismus a liberalismus v americkém pojetí</b><br>
Ve Spojených státech mají oba pojmy definující základní politické směry - liberalismus a konzervatismus - jiný význam než v Evropě.<br><br>
Zatímco <b>konzervatismus</b> obecně odkazuje na zachování starých pořádků a v různých dobách a částech Evropy měl tedy různé podoby - v Británii 19. století třeba podporu monarchie, imperialismu a sociálního státu - ve Spojených státech nabral specifický význam. Většinové ekonomické postoje amerických konzervativců se z evropského pohledu označují za <i>neoliberální</i>. Americký konzervatismus odkazuje na křesťanské a republikánské základy Spojených států; za klíčové priority považuje malý stát, nízké daně a svobodný trh s minimálními vládními zásahy.<br><br>

Z křesťanské tradice vyplývají i <i>sociálně-konzervativní postoje</i> amerických konzervativců - odpor proti potratům a homosexualitě nebo třeba podpora výuky náboženství na školách. V zahraniční politice je mezi konzervativci výrazný směr zvaný <i>neokonzervatismus</i>: silně antikomunistické zaměření, prosazování - i vojenské - národních zájmů Spojených států v zahraničí a podpora Izraele. Největší vliv měl neokonzervatismus za vlády prezidenta George W. Bushe po teroristických útocích 11. září 2001.<br><br>

<b>Liberalismus</b>, ve světě obecně spojovaný s ekonomickou pravicí, má ve Spojených státech především <i>sociální</i> náplň. Pro moderní americké liberály je prioritou rovnoprávnost a sociální spravedlnost; jejich aktuálními tématy jsou práva sexuálních menšin, otázka postavení žen - od platové nerovnosti pro přístup k potratům - a reforma migrační politiky. Liberálové podporují investice do vzdělávání, zdravotní a sociální péče. Jejich ekonomické pozice vycházejí z <i>keynesiánství</i>: věří v tržní ekonomiku, ale zároveň v nutnost státních zásahů v zájmu zachování její stability.
</p>

## 5. Volby 1964: Nový život konzervatismu

Jak se stalo, že tradičně demokratický jih Spojených států obrátil a začal volit republikány? Souvisí tím právě konec „koalice Nového údělu“. Ta se začala hroutit v šedesátých letech, kdy se hlavním společenským tématem stala otázka práv černochů a rasová diskriminace. Demokraté, zastoupení charismatickým Johnem F. Kennedym a po jeho smrti rukou atentátníka v roce 1963 viceprezidentem Lyndonem Johnsonem, většinově podporovali čím dál širší afroamerické hnutí za občanská práva a slibovali nový zákon, který měl ukončit segregaci a černochy a bělochy zrovnoprávnit. Johnson kandidoval v prezidentských volbách roku 1964 jako nositel Kennedyho odkazu.

<figure>
<img src="media/nixon_johnson.jpg" width="100%" alt="Lyndon Johnson (vpravo) a jeho pozdější nástupce Richard Nixon (foto: Yoichi Okamoto, Bílý dům)" title="Lyndon Johnson (vpravo) a jeho pozdější nástupce Richard Nixon (foto: Yoichi Okamoto, Bílý dům)">
<figcaption>
<small><i>
Lyndon Johnson (vpravo) a jeho pozdější nástupce Richard Nixon (foto: Bílý dům)
</i></small>
</figcaption>
</figure>

Z republikánského tábora, rozděleného otázkou občanských práv na umírněné a konzervativní křídlo, byl jako Johnsonův vyzyvatel těsně zvolen konzervativec Barry Goldwater. Goldwater patřil k těm, kteří zákon o zrovnoprávnění černochů nepodporovali, a segregaci považoval za otázku, která spadá do kompetence jednotlivých států. Tím odradil většinu umírněných a liberálních voličů, ale získal jako vůbec první republikán podporu v do té doby demokratických jižních státech - těch, které se o sto let dříve odtrhly od USA kvůli zachování otroctví.

<aside class="big">
  <div class="map" data-year="1964" id="map1964"></div>
</aside>

Goldwater volby výrazným nepoměrem prohrál, ale jeho účast v prezidentských volbách spustila novou vlnu amerického konzervatismu podobně, jako Bryan o šedesát let před ním oživil liberalismus. Konzervativní republikáni začali ve straně postupně získávat převahu a vytlačovat ty umírněné; Goldwaterův úspěch na jihu jim nabídl novou naději a cestu k moci. Navázali na něj takzvanou __jižní strategií__ - získáváním bílých voličů skrz rasová témata a nepřímými náznaky, že plošné ukončení segregace zákonem o občanských právech, který v roce 1964 Johnson skutečně prosadil, byla chyba. Jižní strategie se poprvé vyplatila už o čtyři roky později, kdy v reakci na nové rasové nepokoje slíbil republikánský kandidát Richard Nixon vládu „práva a pořádku“ - a s přehledem zvítězil.

## 6. Volby 1980: Reaganova revoluce

Jižní strategii ovšem nejvíce zúročil kandidát ve volbách roku 1980, jehož prezidentské období zůstává dodnes kontroverzní: Ronald Reagan. Reagan kandidoval proti úřadujícímu prezidentovi Jimmymu Carterovi a zvítězil s takovou převahou, že mohl zahájit konzervativní přerod americké společnosti, kterému se dnes říká __Reaganova revoluce__.

<aside class="big">
  <div class="map" data-year="1980" id="map1980"></div>
</aside>

<aside class="small">
<figure>
<img src="media/reagan.jpg" width="100%" alt="Oficiální portrét Ronalda Reagana (foto: Bílý dům)" title="Oficiální portrét Ronalda Reagana (foto: Bílý dům)">
<figcaption>
<small><i>
Oficiální portrét Ronalda Reagana (foto: Bílý dům)
</i></small>
</figcaption>
</figure>
</aside>

Nepříliš oblíbený prezident Carter se během své vlády musel potýkat s ekonomickou a energetickou krizí, vaz mu ale zlomila až situace okolo amerických rukojmí v Íránu. Po íránské islámské revoluci v roce 1979 zabrala skupina islamistů americkou ambasádu a rok a půl zadržovala 52 Američanů. Carterův pokus o jejich osvobození skončil fiaskem, které ve spojení s neschopností postavit na nohy domácí ekonomiku zapříčinilo jeho volební neúspěch.

Reagan ve své kampani zdůrazňoval potřebu vojenské připravenosti a udržení míru a představil plány na obnovu ekonomiky. V rámci jižní strategie často kritizoval „příjemce sociálních dávek“ - což běloši z jihu chápali jako kódové označení pro černochy. V tehdy mimořádně důležitých televizních debatách ale Reagan svou rétorickou zdatností oslovil voliče ze všech částí Spojených států. Reagan, později znovuzvolený a celých osm let ve funkci mimořádně populární, zavedl opatření, jejichž vliv USA cítí dodnes: pravicové posílení „svobodného trhu“ snížením korporátních daní, masivní investice do armády, válka proti drogám a odpor ke změnám v rozsahu občanských práv. Republikánská strana za Reagana definitivně nabrala svou současnou ekonomicky pravicovou, sociálně konzervativní podobu.

<p class="boxik">
<b>Červené a modré státy</b><br>
Pro dnešní pozorovatele amerických voleb může být překvapením, že všeobecně používané barevné rozlišení amerických stran na červené republikány a modré demokraty je teprve několik let stará novinka. Přinejmenším do osmdesátých let se barvy používaly většinou opačně, podle evropské tradice označovat levicové strany červeně: modrá pro republikány, červená pro demokraty. Praxe se ale v jednotlivých médiích lišila, některá dokonce barvy v rámci vyváženosti každý volební rok střídala.<br><br>

Zvyk označovat republikány červeně a demokraty modře se ustálil až v roce 2000, kdy byly volební mapy i díky kontinuálnímu televiznímu zpravodajství a rozšíření internetu neustále na očích; barevné schéma se tak v zájmu všeobecné srozumitelnosti sjednotilo. Ve stejné době vzniklo i označení „červené a modré státy“ - tedy ty, ve kterých dlouhodobě převažují voliči republikánů, resp. demokratů. Schéma brzy přejaly i samotné strany: dnes mají demokraté modrou a republikáni červenou přímo v logu a „své barvy“ využívají i v propagačních materiálech nebo během sjezdů a volebních kampaní.</p>

## 7. Volby 2016: Vykopané příkopy
Nejnovější proměna dvou amerických stran, nastartovaná v Reaganově éře, je dnes dokončená. Republikány i demokraty už nedefinují ekonomické postoje - demokraté se totiž v devadesátých letech posunuli k republikánům blízké „třetí cestě“, ekonomickému konzervatismu, k němuž se hlásil Bill Clinton a dnes ho [vyznává Barack Obama](http://www.politico.com/story/2009/03/obama-i-am-a-new-democrat-019862) - ale především ty sociální.

<figure>
<img src="media/obama_clinton.jpg" width="100%" alt="Barack Obama a Hillary Clintonová (foto: Pete Souza, Bílý dům)" title="Barack Obama a Hillary Clintonová (foto: Pete Souza, Bílý dům)">
<figcaption>
<small><i>
Barack Obama a Hillary Clintonová (foto: Pete Souza, Bílý dům)
</i></small>
</figcaption>
</figure>

Z republikánské strany prakticky vymizelo liberální křídlo: jako celek se republikáni vymezují proti potratům, stejnopohlavním sňatkům nebo zpřísnění podmínek držení zbraní, podporují tradiční model rodiny a „křesťanské hodnoty“. Její dnešní frakce se liší především mírou sociálního konzervatismu a jeho případným navázáním na náboženství; s kandidaturou Donalda Trumpa se také zejména na internetu objevil extrémní proud takzvané __alternativní pravice__ hlásající bílý nacionalismus a odpor proti imigraci, multikulturalismu a politické korektnosti. Republikáni si v důsledku jižní strategie [drží své pozice](https://www.washingtonpost.com/graphics/politics/2016-election/the-demographic-groups-fueling-the-election/) na jihu země, úspěšní jsou také mezi mormony, protestanty a na venkově.

Demokraté oproti republikánům zastávají v rámci linie nastavené v šedesátých letech sociální liberalismus: prosazují práva pro sexuální menšiny, toleranci k imigraci nebo ochranu životního prostředí. Dříve silná frakce konzervativních demokratů je dnes zanedbatelná a ve straně naopak přibývá těch, kteří odmítají „třetí cestu“ devadesátých let - v loňských volbách byli zastoupeni neúspěšným vyzyvatelem Hillary Clintonové Berniem Sandersem. [Úspěch mají demokraté](https://www.washingtonpost.com/graphics/politics/2016-election/the-demographic-groups-fueling-the-election/) zejména u vysokoškolsky vzdělaných, městských voličů, čím dál početnější černošské populace a také Asiatů a Hispánců.

Volba mezi Hillary Clintonovou a Donaldem Trumpem se tak [občas interpretovala](http://www.vox.com/culture/2016/10/25/13341168/pepe-the-frog-alt-right-scott-adams) jako __kulturní válka__, střet dvou proudů s dramaticky odlišnými představami o fungování společnosti. V tom, který z nich převážil nad tím druhým, dnes mají voliči v Americe a pozorovatelé z celého světa jasno.